﻿using ApiWalletServer.Modules._Core._Self.Controllers;
using ApiWalletServer.Modules._Core._Self.Responses;
using ApiWalletServer.Modules.FAQ._Self.Entities;
using ApiWalletServer.Modules.FAQ._Self.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ
{
    [Authorize]
    [ApiVersion("2")]
    [ApiVersion("2.1")]
    [ApiVersion("2.2")]
    [ApiVersion("2.3")]
    [ApiVersion("2.4")]
    [ApiVersion("2.5")]
    [Route("api/v{version:apiVersion}/faqs")]
    [ApiController]
    public class FAQController : AuthorizeControllerBase
    {
        private readonly IFAQQuestionService _faqQuestionService;

        public FAQController(IFAQQuestionService faqQuestionService)
        {
            _faqQuestionService = faqQuestionService;
        }

        [HttpGet]
        public async Task<IActionResult> FAQ()
        {
            var result = await _faqQuestionService.GetAllAsync();
            if (result == null)
                return new ErrorJsonObjectResult((short)HttpStatusCode.RESOURCE_NOT_FOUND);

            return new SuccessJsonObjectResult(data: result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById([FromRoute] Guid id)
        {
            var result = await _faqQuestionService.GetByIdAsync(id);
            return new SuccessJsonObjectResult(code:(short)HttpStatusCode.OK, data:result );
        }
    }
}
