﻿using ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Extensions
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddFAQAnswerServiceCollection(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IFAQAnswerRepository, FAQAnswerRepository>(); // TODO ->Singelton

            return serviceCollection;
        }
    }
}
