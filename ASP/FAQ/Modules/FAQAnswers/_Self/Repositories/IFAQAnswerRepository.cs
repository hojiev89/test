﻿using ApiWalletServer.Modules._Core._Self.Repositories;
using ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Repositories
{
    public interface IFAQAnswerRepository : IRepositoryBase<FAQAnswer>
    {
        Task<FAQAnswer> GetByQuestionId(Guid questionId);
    }
}
