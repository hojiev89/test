﻿using ApiWalletServer.Modules._Core._Self;
using ApiWalletServer.Modules._Core._Self.Repositories;
using ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Repositories
{
    public class FAQAnswerRepository : RepositoryBase<FAQAnswer>, IFAQAnswerRepository
    {
        public FAQAnswerRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
        public async Task<FAQAnswer> GetByQuestionId(Guid questionId)
        {
            return await _dbContext.FAQAnswers.Where(q => q.FAQQuestionId == questionId ).FirstOrDefaultAsync();
        }
    }
}
