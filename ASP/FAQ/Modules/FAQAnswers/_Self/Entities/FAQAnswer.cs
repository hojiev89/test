﻿using ApiWalletServer.Modules._Core._Self.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Entities
{
    [Table("faq_answers")]
    public class FAQAnswer : EntityBase
    {
        [Column("faq_question_id")]
        public Guid FAQQuestionId { get; set; }

        [Column("body")]
        public string Body { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; }

        [Column("created_at")]
        public DateTime? CreatedAt { get; set; }

        [Column("updated_at")]
        public DateTime? UpdatedAt { get; set; }
    }
}
