﻿using ApiWalletServer.Modules._Core._Self.Helpers;

namespace ApiWalletServer.Modules.FAQ._Self
{
    public class Bootstrap : BootstrapBase
    {
        protected override string GetCurrentNamespace()
        {
            return typeof(Bootstrap).Namespace;
        }
    }
}
