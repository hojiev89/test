﻿using ApiWalletServer.Modules._Core._Self.Entities;

using ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ._Self.Entities
{
    [Table("faq_questions")]
    public class FAQQuestion : EntityBase
    {


        [Column("title")]
        public string Title { get; set; }

        [Column("position")]
        public int Position { get; set; }

        [Column("parent_id")]
        public Guid ParentId { get; set; }

        [Column("is_active")]
        public bool IsActive { get; set; }

        [Column("created_at")]
        public DateTime? CreatedAt { get; set; }

        [Column("updated_at")]
        public DateTime? UpdatedAt { get; set; }
        [NotMapped]
        public ICollection<FAQQuestion> Child { get; set; }

    }
}
