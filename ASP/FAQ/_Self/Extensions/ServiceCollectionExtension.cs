﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiWalletServer.Modules.Cashbacks._Self.Repositories;
using ApiWalletServer.Modules.FAQ._Self.Services;
using ApiWalletServer.Modules.FAQ._Self.Repositories;
using ApiWalletServer.Modules.Orders._Self.Repositories;
using ApiWalletServer.Modules.Orders._Self.Services;
using ApiWalletServer.Modules.Users.Modules.UserSessions._Self.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace ApiWalletServer.Modules.FAQ._Self.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddFAQServiceCollection(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IFAQQuestionRepository, FAQQuestionRepository>();
            serviceCollection.AddTransient<IFAQQuestionService, FAQQuestionService>();
            

            return serviceCollection;
        }
    }
}
