﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ._Self.ModelResponses
{
    public class FAQByIdResponse
    {
        public Guid Id { get; set; }
        public string CODE { get; set; }
        public string DATA { get; set; }

    }
}
