﻿using ApiWalletServer.Modules.Accounts._Self.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiWalletServer.Modules.Accounts.Modules.AccountCategoryTypes._Self.Helpers;
using ApiWalletServer.Modules.Accounts.Modules.AccountTypes._Self.Helpers;
using ApiWalletServer.Modules.Accounts._Self.ModelRequests;
using ApiWalletServer.Modules.Users._Self.ModelResponses;
using ApiWalletServer.Modules._Core._Self.Helpers;
using ApiWalletServer.Modules.FAQ._Self.Entities;

namespace ApiWalletServer.Modules.FAQ._Self.ModelResponses
{
    public class FAQMapping : Profile
    {

        public FAQMapping()
        {


            CreateMap<FAQQuestion, QuestionModelResponse>();

            
            
        }
    }
}
