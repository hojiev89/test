﻿using ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ._Self.ModelResponses
{
    public class QuestionModelResponse
    {
        public Guid Id { get; set; }
        [Column("parent_id")]
        public Guid ParentId { get; set; }
        public string Title { get; set; }
        public List<QuestionModelResponse> Child { get; set; }
    }
}
