﻿using ApiWalletServer.Modules._Core._Self.Repositories;
using ApiWalletServer.Modules.FAQ._Self.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ._Self.Repositories
{
    public interface IFAQQuestionRepository : IRepositoryBase<FAQQuestion>
    {
        Task<FAQQuestion> GetById(Guid id);
        Task<List<FAQQuestion>> GetAllActive();
    }
}
