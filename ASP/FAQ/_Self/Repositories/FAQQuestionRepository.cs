﻿using ApiWalletServer.Modules._Core._Self.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiWalletServer.Modules.FAQ._Self.Entities;
using ApiWalletServer.Modules._Core._Self;
using Microsoft.EntityFrameworkCore;

namespace ApiWalletServer.Modules.FAQ._Self.Repositories
{
    public class FAQQuestionRepository : RepositoryBase<FAQQuestion>, IFAQQuestionRepository
    {
        public FAQQuestionRepository(AppDbContext appDbContext) : base(appDbContext)
        {

        }

        public async Task<List<FAQQuestion>> GetAllActive() 
        {
            return await _dbContext.FAQQuestions
                .Where(c=>c.IsActive)
                .OrderBy(c => c.Position ).ToListAsync();
        }

        public async Task<FAQQuestion> GetById(Guid id)
        {
            return await _dbContext.FAQQuestions.FirstOrDefaultAsync(c => c.Id == id && c.IsActive);
        }
    }
}
