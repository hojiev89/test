﻿using ApiWalletServer.Modules._Core._Self.Exceptions;
using ApiWalletServer.Modules._Core._Self.Helpers;
using ApiWalletServer.Modules.FAQ._Self.Entities;
using ApiWalletServer.Modules.FAQ._Self.ModelResponses;
using ApiWalletServer.Modules.FAQ._Self.Repositories;
using ApiWalletServer.Modules.FAQ.Modules.FAQAnswers._Self.Repositories;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ._Self.Services
{
    public class FAQQuestionService : IFAQQuestionService

    {
        private readonly IMapper _mapper;
        private readonly IFAQQuestionRepository _faqQuestionRepository;
        private readonly IFAQAnswerRepository _faqAnswerRepository;
        private readonly Translate _translate;
        private readonly IConfiguration _configuration;
        public FAQQuestionService(
            IMapper mapper,
            IFAQQuestionRepository faqQuestionRepository,
            IFAQAnswerRepository faqAnswerRepository,
            Translate translate,
            IConfiguration configuration
            )
        {
            _mapper = mapper;
            _faqQuestionRepository = faqQuestionRepository;
            _faqAnswerRepository = faqAnswerRepository;
            _translate = translate;
            _configuration = configuration;
        }
        public async Task<List<QuestionModelResponse>> GetAllAsync()
        {
            var questions = await _faqQuestionRepository.GetAllActive();

            var questionsTree = this.GetQuestionTree(questions);

            return _mapper.Map<List<QuestionModelResponse>>(questionsTree);
        }
        private List<FAQQuestion> GetQuestionTree(ICollection<FAQQuestion> questions)
        {
            foreach (var item in questions)
            {
                item.Child = GetQuestionChildren(questions, item) ?? new List<FAQQuestion>();
            }

            foreach (var item in questions.ToList())
            {
                if (item.ParentId != Guid.Empty)
                {
                    questions.Remove(item);
                    
                }
            }


            return questions.ToList();
        }
        private ICollection<FAQQuestion> GetQuestionChildren(ICollection<FAQQuestion> allQuestions, FAQQuestion question)
        {
            //base case
            //if (allQuestions.All(b => b.ParentId != question.Id)) return null;

            //recursive case
            question.Child = allQuestions
                .Where(b => b.ParentId == question.Id)
                .ToList();

            foreach (var item in question.Child)
            {
                item.Child = GetQuestionChildren(allQuestions, item) ?? new List<FAQQuestion>();

            }

            return question.Child;
        }
        public async Task<string> GetByIdAsync(Guid id)
        {
            var faqQuestion = await _faqAnswerRepository.GetByQuestionId(id);
            if (faqQuestion == null)
            {
                throw new LogicException("Данные не найдены");
            }

            return faqQuestion.Body.ToString();



        }

        
    }
}
