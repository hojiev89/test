﻿using ApiWalletServer.Modules.FAQ._Self.ModelResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWalletServer.Modules.FAQ._Self.Services
{
    public interface IFAQQuestionService
    {
        Task<string> GetByIdAsync(Guid id);
        Task<List<QuestionModelResponse>> GetAllAsync();
    }
}
