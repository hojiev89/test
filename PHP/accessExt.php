<?php
class accessExt{
    public $table='access';


    public static $hidden=             '`access`.`hidden`';
    public static $hidden_R=           '`access`.`hidden` = ';
    public static $hidden_MD5=         'md5(`access`.`hidden`) = ';
    public static $hidden_LIKE=        '`access`.`hidden` like ';
    public static $hidden_IN=          '`access`.`hidden` in ';
    public static $hidden_NOTIN=       '`access`.`hidden` not in ';
    public static $hidden_B=           '`access`.`hidden` > ';
    public static $hidden_BR=          '`access`.`hidden` >= ';
    public static $hidden_M=           '`access`.`hidden` < ';
    public static $hidden_MR=          '`access`.`hidden` <= ';
    public static $hidden_NULL=        'isnull(`access`.`hidden`)=  ';
    public static $hidden_NOTNULL=     'not isnull(`access`.`hidden`)= ';
    public static $hidden_BETWEEN=      '(`access`.`hidden`) ';


    public static $id=             '`access`.`id`';
    public static $id_R=           '`access`.`id` = ';
    public static $id_MD5=         'md5(`access`.`id`) = ';
    public static $id_LIKE=        '`access`.`id` like ';
    public static $id_IN=          '`access`.`id` in ';
    public static $id_NOTIN=       '`access`.`id` not in ';
    public static $id_B=           '`access`.`id` > ';
    public static $id_BR=          '`access`.`id` >= ';
    public static $id_M=           '`access`.`id` < ';
    public static $id_MR=          '`access`.`id` <= ';
    public static $id_NULL=        'isnull(`access`.`id`)=  ';
    public static $id_NOTNULL=     'not isnull(`access`.`id`)= ';
    public static $id_BETWEEN=      '(`access`.`id`) ';


    public static $name=             '`access`.`name`';
    public static $name_R=           '`access`.`name` = ';
    public static $name_MD5=         'md5(`access`.`name`) = ';
    public static $name_LIKE=        '`access`.`name` like ';
    public static $name_IN=          '`access`.`name` in ';
    public static $name_NOTIN=       '`access`.`name` not in ';
    public static $name_B=           '`access`.`name` > ';
    public static $name_BR=          '`access`.`name` >= ';
    public static $name_M=           '`access`.`name` < ';
    public static $name_MR=          '`access`.`name` <= ';
    public static $name_NULL=        'isnull(`access`.`name`)=  ';
    public static $name_NOTNULL=     'not isnull(`access`.`name`)= ';
    public static $name_BETWEEN=      '(`access`.`name`) ';


    public static $sort=             '`access`.`sort`';
    public static $sort_R=           '`access`.`sort` = ';
    public static $sort_MD5=         'md5(`access`.`sort`) = ';
    public static $sort_LIKE=        '`access`.`sort` like ';
    public static $sort_IN=          '`access`.`sort` in ';
    public static $sort_NOTIN=       '`access`.`sort` not in ';
    public static $sort_B=           '`access`.`sort` > ';
    public static $sort_BR=          '`access`.`sort` >= ';
    public static $sort_M=           '`access`.`sort` < ';
    public static $sort_MR=          '`access`.`sort` <= ';
    public static $sort_NULL=        'isnull(`access`.`sort`)=  ';
    public static $sort_NOTNULL=     'not isnull(`access`.`sort`)= ';
    public static $sort_BETWEEN=      '(`access`.`sort`) ';

}?>