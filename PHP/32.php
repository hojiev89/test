<?php
    $userWhere=array(documentExt::$hidden_R=>'0');

    if(strlen(@$act['from'])>0){
        $userWhere[documentExt::$from_R]="{$act['from']}";
    }
    if(strlen(@$act['to'])>0){
        $userWhere[documentExt::$to_R]="{$act['to']}";
    }
    if(strlen(@$act['created_date'])>0){
        $userWhere[documentExt::$created_date_R]="{$act['created_date']}";
    }
    if(strlen(@$act['comment'])>0){
        $userWhere[documentExt::$comment_LIKE]="%{$act['comment']}%";
    }
    if(strlen(@$act['status'])>0){
        $userWhere[documentExt::$status_R]="{$act['status']}";
    }
    if(strlen(@$act['document_type'])>0){
        $userWhere[documentExt::$type_R]="{$act['document_type']}";
    }
     
    $obj= document::get($userWhere, '`created_date` desc');
    
    foreach ($obj as $o) {
        $bodyItem['id']=$o->getId();
        $bodyItem['from']=$o->parentDocumentFrom()->getName();
        $bodyItem['to']=$o->parentDocumentTo()->getName();
        $bodyItem['line']=$o->parentDocumentLine()->getName();
        $bodyItem['document_type']=$o->parentDocumentType()->getName();
        $bodyItem['document_type_id']=$o->getType();
        $bodyItem['created_date']=$o->getCreated_date();
        $bodyItem['created_time']=$o->getCreated_time();
        $bodyItem['comment']=$o->getComment();
        $bodyItem['status']=$o->parentDocumentStatus()->getName();
        $bodyItem['total']=$o->getTotal();
       
        $body[]=$bodyItem;
    }
    echo json_encode($body);
?>
