<?php
    if(isset($_GET['action'])){
        include "{$_GET['get']}_{$_GET['action']}.php";
    }else{
        notification();
        include "11_filter_ui.php";
?>
<div class='row'>
    <div class='col-md-12 dash-left'>
        <div class='panel'>
            <div class='panel-heading'>
                <h4 class='panel-title'><a href='<?php echo linkAdd()?>' id='action-add-color'>Добавить</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='main.php?get=11&action=conv<?php gen_key("{$_GET['get']}")?>' id='action-add-color'>Конвертация</a></h4>
            </div>
            
            <div class='panel-body'>
                <div class='table-responsive'> 
                    <table id='dataTable11' class='table table-bordered table-striped-col'>
                    <thead><tr><th>№</th><th>От</th><th>Кому</th><th>Папка</th><th>Тип документа</th><th>Валюта</th><th>Дата соз.</th><th>Время соз.</th><th>Комент</th><th>Статус</th><th>Итого</th><th>Дейст.</th></tr></thead><tbody>
        
                        </tbody>
                        <tfoot><tr><th colspan="10">Итого</th><th></th><th></th></tr></tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>