    <?php
        $edit=new document(intval($_GET['id']));
        if($edit->getStatus()==document_status_otkazan || $edit->getStatus()==document_status_providen){
            $_SESSION['notification']['message']='Неудачно.';
            $_SESSION['notification']['error']='1';
            unset($_GET['action']);
            include "{$_GET['get']}.php";
        }else{
    ?>
<div class='row'>
    <div class='col-md-6'>
        <div class='panel'>
            <div class='panel-heading nopaddingbottom'>
              <h4 class='panel-title'>Редактировать</h4>
            </div>
            <div class='panel-body nopaddingtop'>
              <hr>
              <form id='basicForm4' class='form-horizontal' action='' method='post' onsubmit="return confirm('Вы действительно хотите подвердить?');">
                    <div class='form-group'><label class='col-sm-3 control-label'>От <span class='text-danger'></span></label>
                        <div class='col-sm-8'>
                            <select id='from' class='select2 select_new' name='from' style='width: 100%' required=''>
                                <?php 
                                    $userWhere=array(userExt::$access_NOTIN=>array(access_magazin),'or'=>array(userExt::$is_active_R=>"1","{$_SESSION['person_active']}"=>"1"));
                                    echo user::getOpt($edit->getFrom(), $userWhere, '`id`', '`name`', '`sort`'); 
                                ?>
                            </select>
                            <label class='error' for='from'></label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Сумма <span class='text-danger'></span></label>
                        <div class='col-sm-8'><input type='number' step='any' name='total'  id='total'  value='<?php echo $edit->getTotal()?>' class='form-control' placeholder='Type your info...'  /></div>
                    </div>
                   
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Комент <span class='text-danger'></span></label>
                        <div class='col-sm-8'><input type='text'  name='comment'  id='comment'  value='<?php echo $edit->getComment()?>' class='form-control' placeholder='Type your info...'  /></div>
                    </div>
                    <div class='form-group'><label class='col-sm-3 control-label'>Статус <span class='text-danger'></span></label>
                        <div class='col-sm-8'>
                            <select id='status' class='select2 select_new' name='status' style='width: 100%' required=''>
                                <?php
                                    $userWhere=array(document_statusExt::$id_IN=>array(document_status_otkazan,document_status_na_podverjdeniya,document_status_na_obrabotky));
                                    echo document_status::getOpt($edit->getStatus(), $userWhere, '`id`', '`name`', '`name`'); 
                                ?>
                            </select>
                            <label class='error' for='status'></label>
                        </div>
                    </div>
                
                <hr>
                <div class='row'>
                    <div class='col-sm-9 col-sm-offset-3'>
                        <button class='btn btn-quirk btn-wide btn-primary mr5' name='edit'>Ок</button>
                        <button type='reset' class='btn btn-quirk btn-wide btn-default' onclick="window.location.href='main.php?get=<?php echo $_GET['get'].gen_key("{$_GET['get']}")?>'">Отмена</button>
                    </div>
                </div>
                <input type='hidden' name='id' value='<?php echo intval($_GET['id'])?>'>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
        }
?>