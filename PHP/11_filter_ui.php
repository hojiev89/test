
<div class='col-md-12'>
    <div class='panel panel-default'>
        <div id='myload-body'></div>
        <ul class='panel-options'>
            <li><a><i class='fa fa-refresh'></i></a></li>
            <li><a class='panel-minimize'><i class='fa fa-chevron-down'></i></a></li>
            <li><a class='panel-remove'><i class='fa fa-remove'></i></a></li>
        </ul>
        <div class='panel-heading'><h4 class='panel-title'>Фильтр</h4></div>
        <div class='panel-body' >
            <form id='basicForm4' class='form-horizontal' action='' method='post' onsubmit='return false;'>
                <div class='form-group'>
                    <div class='col-sm-2'>
                        <select id='from' class='select2 select_new' name='from' data-placeholder='От' style='width: 100%'>
                            <?php 
                                $userWhere=array(userExt::$access_NOTIN=>array(access_magazin),'or'=>array(userExt::$is_active_R=>"1","{$_SESSION['person_active']}"=>"1"));
                                echo user::getOpt(null, $userWhere, '`id`', '`name`', '`sort`'); 
                            ?>
                        </select>
                        <label class='error' for='from'></label>
                    </div>
                
                    <div class='col-sm-2'>
                        <select id='to' class='select2 select_new' name='to' data-placeholder='Кому' style='width: 100%' >
                            <?php 
                                $userWhere=array(userExt::$access_NOTIN=>array(access_magazin),'or'=>array(userExt::$is_active_R=>"1","{$_SESSION['person_active']}"=>"1"));
                                echo user::getOpt(null, $userWhere, '`id`', '`name`', '`sort`'); 
                            ?>
                        </select>
                        <label class='error' for='to'></label>
                    </div>
                
                    <div class='col-sm-2'>
                        <input type='date' name='created_date' id='created_date' class='form-control' placeholder='Дата соз.'  value="<?php echo date('Y-m-d')?>"/>
                    </div>
                    
                    <div class='col-sm-2'>
                        <input type='text'  name='comment' id='comment' class='form-control' placeholder='Комент'  />
                    </div>
                    
                    <div class='col-sm-2'>
                        <select id='status' class='select2 select_new' name='status' data-placeholder='Статус' style='width: 100%' >
                            <?php echo document_status::getOpt(null, null, '`id`', '`name`', '`name`'); ?>
                        </select>
                        <label class='error' for='status'></label>
                    </div>
                
            </div>
            <hr>
                <div class='row'>
                    <div class='col-sm-9 col-sm-offset-3'>
                        <button class='btn btn-quirk btn-wide btn-primary mr5' name='11_filter' id='11_filter' >Ок</button>
                        <button type='reset' class='btn btn-quirk btn-wide btn-default' id='11_cancel'>СБРОС</button>
                    </div>
                </div>
        </form>
    </div>
</div>
</div>

