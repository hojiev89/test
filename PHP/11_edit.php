<?php
    if(isset($_POST['edit'])){
        mysqli_query($_SESSION['link'],"START TRANSACTION");
        $edit=new document($_POST['id']);
        $edit->setTotal($_POST['total']);
        $edit->setTotal_val($_POST['total']);
        $edit->setFrom($_POST['from']);
        $edit->setComment($_POST['comment']);
        $edit->setStatus($_POST['status']);
        $res=array();
        applay($edit, $res);
        if(!in_array(FALSE, $res)){
            mysqli_query($_SESSION['link'],'COMMIT');
            $_SESSION['notification']['message']='Успешно.';
            $_SESSION['notification']['error']='0';
        }else {
            mysqli_query($_SESSION['link'],'ROLLBACK');
            $_SESSION['notification']['message']='Неудачно.';
            $_SESSION['notification']['error']='1';
        }
        unset($_GET['action']);
        include "{$_GET['get']}.php";
    }else{
        include 'index/11_edit_ui.php';
    }
?>
