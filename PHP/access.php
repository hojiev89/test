<?php
class access{
    public $table='access';

    private $hidden;
   private $hidden_flag=-1;

    private $id;
   private $id_flag=-1;

    private $name;
   private $name_flag=-1;

    private $sort;
   private $sort_flag=-1;


    public function getHidden() {
        return $this->hidden;
    }

    public function setHidden($hidden) {
        $this->hidden_flag += 1;
        $this->hidden = $hidden;
    }


    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id_flag += 1;
        $this->id = $id;
    }


    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name_flag += 1;
        $this->name = $name;
    }


    public function getSort() {
        return $this->sort;
    }

    public function setSort($sort) {
        $this->sort_flag += 1;
        $this->sort = $sort;
    }


    function __construct($id=NULL,$where=NULL,$res=NULL) 
    { 
        $a = func_get_args(); 
        $i = func_num_args(); 
        if (method_exists($this,$f='__construct'.$i)) { 
            call_user_func_array(array($this,$f),$a); 
        } 
    } 
    
    function __construct0() 
    { 
        $this->hidden_flag += 1;
$this->id_flag += 1;
$this->name_flag += 1;
$this->sort_flag += 1;
    } 

    function __construct1($id) 
    { 
        $this->load($id);
    } 

    function __construct2($id,$where) 
    { 
        $this->load($id, doc::userWhere($where));
    }
    
    function __construct3($id,$where,$res) 
    { 
        $this->set($res);
    }

    private function load($id=NULL,$where=NULL){
        $w=" `access`.`hidden`='0'  "; 
        if(!is_null($id) or !is_null($where)){
            if(!is_null($where))$w.=" and {$where} ";
            if(!is_null($id))$w.=" and `id`='{$id}' ";
                
            $result=doc::myselect($this->table, "*", $w,NULL,NULL,1);
            if(mysqli_num_rows($result)>0){
                while ($row = mysqli_fetch_array($result)) {
                    $this->set($row);
                }
            }else{
                $this->hidden_flag += 1;
$this->id_flag += 1;
$this->name_flag += 1;
$this->sort_flag += 1;
            }
        }else{
            $this->set($id);
        }
    }
    
    private function set($row){
        $this->setHidden($row['hidden']);
        $this->setId($row['id']);
        $this->setName($row['name']);
        $this->setSort($row['sort']);
    }
    public function save($ins=false){
    $val=array();
            if($this->hidden_flag>=1)$val['hidden']=doc::kavichka($this->getHidden());
        if($this->id_flag>=1)$val['id']=doc::kavichka($this->getId());
        if($this->name_flag>=1)$val['name']=doc::kavichka($this->getName());
        if($this->sort_flag>=1)$val['sort']=doc::kavichka($this->getSort());
    $this->setId(doc::sqlExec('access',$val,$this->getId(),$ins));
    return $this->getId();
    }
    
    
    public static function get($userWhere=NULL,$orderby=null,$groupby=NULL,$limit=NULL,$access=false){
        $where=" `access`.`hidden`='0'  "; 
        if(!is_null($userWhere))$where.=" and ".doc::userWhere($userWhere);
        if(is_null($orderby))$orderby=' `id` ';
        
        $ressult=doc::myselect('`access`', '`access`.*', $where, $orderby, $groupby, $limit,$access);
        $list=array();
        if(mysqli_num_rows($ressult)>0){
            while ($row = mysqli_fetch_array($ressult)) {
                $list[]=new access(null,null,$row);
            }
        }
        return $list;
    }
    
    public static function getAll($userWhere=NULL,$orderby=null,$groupby=NULL,$limit=NULL,$access=false){
        $where=""; 
        if(!is_null($userWhere))$where=doc::userWhere($userWhere);
        if(is_null($orderby))$orderby=' `id` ';
        
        $ressult=doc::myselect('`access`', '`access`.*', $where, $orderby, $groupby, $limit,$access);
        $list=array();
        if(mysqli_num_rows($ressult)>0){
            while ($row = mysqli_fetch_array($ressult)) {
                $list[]=new access(null,null,$row);
            }
        }
        return $list;
    }
    
    public static function getJoin($userWhere=NULL,$sql=null,$orderby=null,$groupby=NULL,$limit=NULL,$access=false){
        $where=" access.`hidden`='0' ";
        $join=null;
        if(!is_null($userWhere))$where.=" and ".doc::userWhere($userWhere);
        if(!is_null($sql))$join=$sql->sql();
        if(is_null($orderby))$orderby=' `id` ';
        
        $ressult=doc::myselect('`access`', '`access`.* ' , $where, $orderby, $groupby, $limit,$access,$join);
        $list=array();
        if(mysqli_num_rows($ressult)>0){
            while ($row = mysqli_fetch_array($ressult)) {
                $list[]=new access(null,null,$row);
            }
        }
        return $list;
    }
    
    public static function getJoinArr($userWhere=NULL,$sql=null,$orderby=null,$groupby=NULL,$limit=NULL,$access=false,$col=null){
        $where="( `access`.`hidden`='0' or `access`.`hidden` is null ) "; 
        $join=null;
        if(!is_null($userWhere))$where.=" and ".doc::userWhere($userWhere);
        if(!is_null($sql))$join=$sql->sql();
        if(is_null($orderby))$orderby=' `id` ';
        
        if(!is_null($sql)&& !is_null($col)){
            $cl= implode(",", get_alia_var($col));
            $ressult=doc::myselect('`access`', "`access`.*,{$cl} " , $where, $orderby, $groupby, $limit,$access,$join);
        }
        else{
            $ressult=doc::myselect('`access`', '`access`.* ' , $where, $orderby, $groupby, $limit,$access,$join);
        }
        $list=array();
        if(mysqli_num_rows($ressult)>0){
            while ($row = mysqli_fetch_array($ressult)) {
                $list[]=$row;
            }
        }
        return $list;
    }

    public static function getArr($userWhere=NULL,$col='id',$access=false){
        $where=" `access`.`hidden`='0' "; 
        if(!is_null($userWhere))$where.=" and ".doc::userWhere($userWhere);
        
        $ressult=doc::myselect('`access`', '`access`.*', $where,null,null,null,$access);
        $list=array();
        if(mysqli_num_rows($ressult)>0){
            while ($row = mysqli_fetch_array($ressult)) {
                $list[]=$row[$col];
            }
        }
        $list[]=0;
        return $list;
    }

    public static function getSumUseIndex($userWhere=NULL,$col='id',$index=null,$access=false){
        $where=" `access`.`hidden`='0' "; 
        if(!is_null($userWhere))$where.=" and ".doc::userWhere($userWhere);
        $ressult=doc::myselectUseIndex('`access`', "sum(`access`.{$col}) {$col}" , $where,null,null,null,$access,null,$index);
        if(mysqli_num_rows($ressult)>0){
            while ($row = mysqli_fetch_array($ressult)) {
                return $row[$col];
            }
        }
        
        return 0;
    }
    
    public static function getOpt($selected=null,$userWhere=NULL,$val='`id`',$cols='*',$orderby=null,$groupby=NULL,$limit=NULL,$access=false){
        $where=" `access`.`hidden`='0' ";
        if(!is_null($userWhere))$where.=" and ".doc::userWhere($userWhere);
        if(is_null($orderby))$orderby=' `id` ';
        $ressult=doc::myselect("access", "{$val} as `id`,`access`.{$cols} as `caption`", $where, $orderby,null,null,$access);
        $list="<option></option>";
        if(mysqli_num_rows($ressult)>0){
            while ($row = mysqli_fetch_array($ressult)) {
                $sel="";
                if(is_array($selected)){
                    if(in_array($row['id'], $selected)){
                        $sel=" selected ";
                    }
                }else{
                    if($row['id']== $selected){
                        $sel=" selected ";
                    }
                }
                $list.="<option {$sel} value='".$row['id']."'>{$row['caption']}</option>";
            }
        }
        return $list;
    }

        public function childSys_accessAccess($userWhere=array(),$order=null,$access=false){
            if(is_null($userWhere))$userWhere=array(); 
            $userWhere=array_merge($userWhere,array(sys_accessExt::$access_R=>  $this->getId()));
            return sys_access::get($userWhere,$order,null,null,$access);
        }    
        
        public function childSys_in_stockAccess($userWhere=array(),$order=null,$access=false){
            if(is_null($userWhere))$userWhere=array(); 
            $userWhere=array_merge($userWhere,array(sys_in_stockExt::$access_R=>  $this->getId()));
            return sys_in_stock::get($userWhere,$order,null,null,$access);
        }    
        
        public function childUserAccess($userWhere=array(),$order=null,$access=false){
            if(is_null($userWhere))$userWhere=array(); 
            $userWhere=array_merge($userWhere,array(userExt::$access_R=>  $this->getId()));
            return user::get($userWhere,$order,null,null,$access);
        }    
        }?>