
<div class='row'>
    <div class='col-md-6'>
        <div class='panel'>
            <div class='panel-heading nopaddingbottom'>
              <h4 class='panel-title'>Добавить</h4>
            </div>
            <div class='panel-body nopaddingtop'>
              <hr>
              <form id='basicForm4' class='form-horizontal' action='' method='post' onsubmit="return confirm('Вы действительно хотите подвердить?');">
                  <input type='hidden'  name='uuid' id='uuid' class='form-control' value="<?php echo doc::generateUuid()?>"  />
                    <div class='form-group'><label class='col-sm-3 control-label'>От <span class='text-danger'></span></label>
                        <div class='col-sm-8'>
                            <select id='from' class='select2 select_new' name='from' style='width: 100%' required=''>
                                <?php 
                                    $userWhere=array(userExt::$access_NOTIN=>array(access_magazin),'or'=>array(userExt::$is_active_R=>"1","{$_SESSION['person_active']}"=>"1"));
                                    echo user::getOpt(null, $userWhere, '`id`', '`name`', '`sort`'); 
                                ?>
                            </select>
                            <label class='error' for='from'></label>
                        </div>
                    </div>
                  
                    <div class='form-group'><label class='col-sm-3 control-label'>Валюта <span class='text-danger'></span></label>
                        <div class='col-sm-8'>
                            <select id='currency' class='select2 select_new' name='currency' style='width: 100%' required=''>
                                <?php 
                                    echo currency::getOpt(valuta_tjs, NULL, '`id`', '`name`', '`sort`'); 
                                ?>
                            </select>
                            <label class='error' for='currency'></label>
                        </div>
                    </div>
                
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Сумма <span class='text-danger'></span></label>
                        <div class='col-sm-8'><input type='number' step='any' name='total' id='total' class='form-control' placeholder='Type your info...'  required=""/></div>
                    </div>
                    
                
                    <div class='form-group'>
                        <label class='col-sm-3 control-label'>Комент <span class='text-danger'></span></label>
                        <div class='col-sm-8'><input type='text'  name='comment' id='comment' class='form-control' placeholder='Type your info...'  /></div>
                    </div>
                    
                    
                    
                <hr>
                <div class='row'>
                    <div class='col-sm-9 col-sm-offset-3'>
                        <button class='btn btn-quirk btn-wide btn-primary mr5' name='add'>Ок</button>
                        <button type='reset' class='btn btn-quirk btn-wide btn-default' onclick="window.location.href='main.php?get=<?php echo $_GET['get'].gen_key("{$_GET['get']}")?>'">Отмена</button>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
</div>