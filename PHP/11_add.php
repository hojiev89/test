<?php
    $res=array();    
    if(isset($_POST['add'])){
        doc::startTransaction();
        $add=new document();
        $add->setType(doctype_pri_cash);
        $add->setTotal($_POST['total']);
        $add->setTotal_val($_POST['total']);
        $add->setUuid($_POST['uuid']);
        $add->setFrom($_POST['from']);
        $add->setTo($_SESSION['auth']->getId());
        $add->setCreated_date(date('Y-m-d'));
        $add->setCreated_time(date('H:i:s'));
        $add->setComment($_POST['comment']);
        $add->setStatus(document_status_na_obrabotky);
        $add->setCurrency_val($_POST['currency']);
        applay($add, $res);
        if(!in_array(FALSE, $res)){
            doc::commit();
            $_SESSION['notification']['message']='Успешно.';
            $_SESSION['notification']['error']='0';
        }else {
            doc::rollBack();
            $_SESSION['notification']['message']='Неудачно.';
            $_SESSION['notification']['error']='1';
        }
        unset($_GET['action']);
        include "{$_GET['get']}.php";
    }else{
        include 'index/11_add_ui.php';
    }
?>
