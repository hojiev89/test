<?php
// Построить вложенное меню в формате HTML
$menu = [
[
    "label" => "Категория 1",
    "url" => "https://domain.local/cat_1"
],
[
    "label" => "Категория 2",
    "url" => "https://domain.local/cat_2"
],
[
    "label" => "Категория 3",
    "url" => "https://domain.local/cat_3"
],
[
    "label" => "Категория 4",
    "items" => [
        [
            "label" => "Категория 4-1",
            "url" => "https://domain.local/cat_4_1"
        ],
        [
            "label" => "Категория 4-2",
            "items" => [
                [
                    "label" => "Категория 4-2-1",
                    "url" => "https://domain.local/cat_4_1"
                ],
                [
                    "label" => "Категория 4-2-2",
                    "url" => "https://domain.local/cat_4_2"
                ],
            ]
        ],
    ]
],
];

$myMenu="";
foreach ($menu as $key => $value) {
  if(isset($value['items'])){
    $str=makeSubIfExistsItems($value['items']);
    $myMenu.="<li>{$value['label']}{$str}</li>";
  }else{
      $myMenu.= "<li><a href='{$value['url']}'>{$value['label']}</a></li>";
  }
}
echo "<ul>{$myMenu}</ul>";

################
function makeSubIfExistsItems($items,$level=1){
  $submenu="";

  foreach ($items as $subkey => $subvalue) {
    if(isset($subvalue['items'])){
      $level++;
      $str=makeSubIfExistsItems($subvalue['items'],$level);
      $submenu.="<li>{$subvalue['label']}</li><ul>{$str}</ul></li>";
    }else{
      $tab=str_repeat("->",$level);
      $submenu.= "<li>{$tab}<a href='{$subvalue['url']}'>{$subvalue['label']}</a></li>";
    }
  }
  return $submenu;
}
